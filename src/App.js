import React from 'react';
import './App.css';
import Header from './components/Header';
import Main from './components/Main';
import Books from './components/Books';
import Newsletter from './components/Newsletter';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
      <Books />
      <Newsletter />
      <Footer />
    </div>
  );
}

export default App;
