import React, { useState } from 'react';
import facebook from '../../assets/facebook.png';
import twitter from '../../assets/twitter.png';
import google from '../../assets/google.png';
import pinterest from '../../assets/pinterest.png';
import './style.css';

export default function Newsletter() {
    const [ email, setEmail ] = useState('');

    const payload = {
        nome_site: "Pixter - Books",
        email
    }

    function handleSubmit() {
       
    }

    return (
        <section className="newsletter" id="newsletter">
            <div className="newsletter__title">
                <p>Keep in touch with us</p>
            </div>
            <div className="newsletter__subtitle">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget
                    tellus tristique bibendum. Donec retrum sed sem quis venenatis.
                </p>
            </div>
            <div className="newsletter__form">
                <form onSubmit={handleSubmit}>
                    <input 
                        type="text" 
                        name="email" 
                        id="email" 
                        placeholder="Enter your email to update"
                        value={email}
                        onChange={event => setEmail(event.target.value)} 
                    />
                    <button type="submit">SUBMIT</button>
                </form>
            </div>
            <div className="newsletter__follow">
                <img src={facebook} alt="Facebook"/>
                <img src={twitter} alt="Twitter"/>
                <img src={google} alt="Google"/>
                <img src={pinterest} alt="Pinterest"/>
            </div>
        </section>
    );
}
