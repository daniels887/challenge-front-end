import React from 'react';
import './style.css'
import ipad from '../../assets/ipad.png';

export default function Main() {
    return (
        <>
            <section className="main" id="main"> 
                <div className="main__text">
                    <div className="main__text__title">
                        <h1>Pixter Digital Books</h1>
                    </div>
                    <div className="main__text__subtitle">
                        <p>Lorem ipsum dolor sit amet?</p>
                        <p>consectetur elit, volupat.</p>
                    </div>
                    <div className="main__text__description">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
                        <p>
                            Praesent vitae eros eget tellus tristique bibendum. Donec
                        </p>
                        <p>retrum sed sem quis venenatis. Proin viverra risus a eros</p>
                        <p>volutpat tempor. In quis arcu et eros porta lobortis sit</p>
                    </div>
                    <div className="main__text__icons">
                    <i className="fa fa-apple"></i>
                    <i className="fa fa-android"></i>
                    <i className="fa fa-windows"></i>
                    </div>
                </div>
                <div className="main__img">
                    <img src={ipad} alt="Ipad" />
                </div>
            </section>
            <div className="list">
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </>
  );
}
