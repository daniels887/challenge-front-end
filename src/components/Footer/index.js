import React from 'react';

import './style.css';

export default function Footer() {
    return (
        <footer className="address" id="address">
            <div className="address__text">
                <p>
                    Alameda Santos, 1978 
                    6th floor - Jardim Paulista 
                    São Paulo - SP
                    +55 11 3090 8500
                </p>
                <p>
                    London - UK
                    125 Kingsway
                    London W2CB 6NH
                </p>
                <p>
                    Lisbon - Portugal
                    Rua Rodrigues Faria, 103
                    4th floor
                    Lisbon - Portugal
                </p>
                <p>
                    Curitiba - PR
                    R. Fransico Rocha, 198
                    Batel - Curitiba - PR
                </p>
                <p>
                    Buenos Aires - Argentina
                    Esmeralda 950
                    Buenos Aires B C1007
                </p>
            </div>
        </footer>
    );
}
