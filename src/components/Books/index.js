import React, { useState, useEffect } from 'react';

import './style.css';

import api from '../../services/api';
import book_image from '../../assets/book_not_found.jpg';
import load from '../../assets/loading.gif';
import Modal from '../../components/Modal';

export default function Books() {

    const [ loading, setLoading ] = useState(true);
    const [ books, setBooks ] = useState([]);
    const [ modal, setModal ] = useState(null);

    useEffect(() => {
        (async function loadBooks() {
            const response = await api.get('volumes?q=PERCY%20JACKSON&maxResults=8');
    
            const { items } = response.data;
            setBooks(items);
            setLoading(false);
        })()
    }, [])
    
    function openModal(book) {
        setModal(book);
    }

    function closeModal() {
        setModal(null);

    }

    return (
        <section className="books" id="books">
            <div className="books__title">
                <h1>Books</h1>
            </div>
            <div className="books__description">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget
                    tellus tristique bibendum. Donec retrum sed sem quis venenatis.
                </p>
            </div>
            <div className="books__thumbnails">
            {loading ? (
                <div className="books__thumbnail__loading">
                    <img src={load} alt="Loading" />
                </div>
              ) : (
                books.map(book => (
                  <div className="books__thumbnails__img" key={book.id} onClick={() => openModal(book)}>
                    <img 
                        src={
                            book.volumeInfo.imageLinks === undefined
                            ? book_image
                            : book.volumeInfo.imageLinks.thumbnail
                        } 
                        alt="Percy Jackson" 
                    />
                  </div>
                ))
              )}
            </div>
            { modal ? ( 
            <Modal close={closeModal}>
                <div>
                    <a href={modal.volumeInfo.canonicalVolumeLink} target="_blank" rel="noopener noreferrer">
                        <img
                        src={
                            modal.volumeInfo.imageLinks === undefined
                            ? book_image
                            : modal.volumeInfo.imageLinks.thumbnail
                        }
                        alt={modal.volumeInfo.title}
                        title="Clique aqui para ver o livro!"
                        />
                    </a>
                    <h1>{modal.volumeInfo.title}</h1>
                    <p>Por: {modal.volumeInfo.authors ? modal.volumeInfo.authors.map(author => `${author} `) : "Sem informação"} </p>
                    <p>Número de páginas: {modal.volumeInfo.pageCount ? modal.volumeInfo.pageCount : "Sem informação"}</p>
                    <p>Editora: {modal.volumeInfo.publisher ? modal.volumeInfo.publisher : "Sem informação"} </p>
                    <p>
                    Publicado em:{" "}
                    {modal.volumeInfo.publishedDate ? new Date(modal.volumeInfo.publishedDate).toLocaleDateString() : "Sem informação"}
                    </p>
                    <div>
                        <p>{modal.volumeInfo.description ? modal.volumeInfo.description : "Sem informação"}</p>
                    </div>
                </div>
            </Modal>
            ) : null}
        </section>
        );
    }
    