import React from 'react';

import './style.css';

export default function Modal(props) {
  return (
    <>
        <div className="modal">
          <div className="modal__container">
            <i onClick={props.close} className="fa fa-close modal__container__close" />
            <div className="modal__container__body">{props.children}</div>
          </div>
        </div>
    </>
  );
}
