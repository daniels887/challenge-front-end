import React, { useState } from 'react';
import logo from '../../assets/fingerprint.png';
import logo_retina from '../../assets/logo-retina-2.png';
import './style.css';

export default function Header() {
    const [ toogleMenu, setToogleMenu ] = useState(false);

    function handleToogleMenu() {
        setToogleMenu(!toogleMenu);
    }

    return (
        <header>
            <div className="logo">
                <img src={logo} alt="Pixter"/>
                <img src={logo_retina} alt="Pixter"/>
            </div>
            <div className="bar" onClick={handleToogleMenu}>
                <i className="fa fa-bars"></i>
            </div>
            <nav className="navbar">
                <ul>
                    <li><a href="#books">Books</a></li>
                    <li><a href="#newsletter">Newsletter</a></li>
                    <li><a href="#address">Address</a></li>
                </ul>
            </nav>
            { toogleMenu && (
                <nav className="navbar__mobile">
                    <ul>
                        <li><a href="#books">Books</a></li>
                        <li><a href="#newsletter">Newsletter</a></li>
                        <li><a href="#address">Address</a></li>
                    </ul>
                </nav>
            )}
        </header>
    );
}
